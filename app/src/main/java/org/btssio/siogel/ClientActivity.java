package org.btssio.siogel;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ClientActivity extends Activity {
    TextView texte;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        texte = (TextView)this.findViewById(R.id.affClient);

        ClientDAO clientDAO = new ClientDAO(this);
        clientDAO.updateClient(3, "telephone", "0603972432");
        Client leClient1 = clientDAO.getClient(1);
        texte.setText(leClient1.toString());

    }
}