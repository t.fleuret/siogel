package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class ModificationClient extends Activity implements OnClickListener {
    TextView tid, nom, prenom, tel, adresse, cp, ville, datecommande, heuresouhaite;
    EditText DateLivraison, heureLivraison, Etat;
    Button annuler, Enregistrer, geoloc, signer, voir_signature;
    ClientDAO dao = new ClientDAO(this);
    Client c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modification_client);
        int id = this.getIntent().getExtras().getInt("id");
        c = dao.getClient(id);
        tid = (TextView) findViewById(R.id.mdfId);
        nom = (TextView) findViewById(R.id.mdfNom);
        prenom = (TextView) findViewById(R.id.mdfPrenom);
        tel = (TextView) findViewById(R.id.mdfTel);
        adresse = (TextView) findViewById(R.id.mdfAdresse);
        cp = (TextView) findViewById(R.id.mdfCP);
        ville = (TextView) findViewById(R.id.mdfVille);
        datecommande = (TextView) findViewById(R.id.mdfDC);
        heuresouhaite = (TextView) findViewById(R.id.mdfHS);

        DateLivraison = (EditText) findViewById(R.id.mdfDL);
        heureLivraison = (EditText) findViewById(R.id.mdfHL);
        Etat = (EditText) findViewById(R.id.Etat);

        annuler = (Button) findViewById(R.id.Annuler);
        Enregistrer = (Button) findViewById(R.id.enregistrer);
        geoloc = (Button) findViewById(R.id.Geoloc);
        signer = (Button) findViewById(R.id.Signer);
        voir_signature = (Button) findViewById(R.id.v_signature);

        geoloc.setOnClickListener(this);
        annuler.setOnClickListener(this);
        Enregistrer.setOnClickListener(this);
        signer.setOnClickListener(this);
        voir_signature.setOnClickListener(this);

        tid.setText("" + c.getIdentifiant());
        nom.setText(c.getNom());
        prenom.setText(c.getPrenom());
        tel.setText(c.getTelephone());
        adresse.setText(c.getAdresse());
        cp.setText(c.getCodePostal());
        ville.setText(c.getVille());
        datecommande.setText(c.getDateCommande());
        heuresouhaite.setText(c.getHeureSouhaitLiv());

        DateLivraison.setText(c.getDateLivraison());
        heureLivraison.setText(c.getHeureReelleLivraison());
        Etat.setText("" + c.getEtat());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Annuler:
                finish();
                break;

            case R.id.enregistrer:
                enregistrer();
                break;
            case R.id.Geoloc:
                geo();
                break;
            case R.id.Signer:
                enregistrer();
                signer();
                break;
            case R.id.v_signature:
                enregistrer();
                v_signer();
                break;
        }
    }

    public void enregistrer() {
        ClientDAO dao = new ClientDAO(this);
        dao.updateClient(c.getIdentifiant(), "dateLivraison", DateLivraison.getText().toString());
        dao.updateClient(c.getIdentifiant(), "heureReelleLivraison", heureLivraison.getText().toString());
        dao.updateClient(c.getIdentifiant(), "etat", Etat.getText().toString());
        finish();
    }

    Intent intentGeo;
    public void geo() {
        intentGeo = new Intent(this, Geolocalisation.class);
        intentGeo.putExtra("adresse", c.getAdresse());
        intentGeo.putExtra("cp", c.getCodePostal());
        intentGeo.putExtra("ville", c.getVille());
        this.startActivity(intentGeo);
    }

    Intent Signer;
    public void signer() {
        Signer = new Intent(this, CaptureSignature.class);
        Signer.putExtra("id", c.getIdentifiant());
        this.startActivity(Signer);
    }

    Intent voir_Signer;
    public void v_signer() {
        if (c.getSignatureBase64().length() != 0) {
            voir_Signer = new Intent(this, AfficheSignature.class);
            voir_Signer.putExtra("id", c.getIdentifiant());
            this.startActivity(voir_Signer);
        }
    }
}