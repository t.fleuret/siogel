package org.btssio.siogel;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.renderscript.Sampler;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ClientDAO {
    private static String base = "BDClient";
    private static int version = 3;
    private BdSQLiteOpenHelper accesBD;

    public ClientDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);
    }
    public List<Client> getAll(){
        List<Client> listeClients = new ArrayList<Client>();
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from client ;",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            do{
                listeClients.add(new Client(curseur.getInt(0),curseur.getString(1), curseur.getString(2),curseur.getString(3),
                        curseur.getString(4),curseur.getString(5),curseur.getString(6),curseur.getString(7),
                        curseur.getString(8),curseur.getString(9),curseur.getString(10),curseur.getString(11),
                        curseur.getInt(12)));
            }
            while (curseur.moveToNext());
        }
        return listeClients;
    }
     

    public void addClient1(Client unClient ){
        SQLiteDatabase bd = accesBD.getWritableDatabase();

        String req = "insert into client"
                + " values('"+unClient.getNom()+"',"+unClient.getPrenom()+"',"+unClient.getAdresse() +
                "',"+unClient.getCodePostal()+"',"+unClient.getVille()+"',"+unClient.getTelephone()+"',"+
                unClient.getDateCommande()+"',"+unClient.getHeureSouhaitLiv()+"',"+unClient.getHeureReelleLivraison()+"',"+
                unClient.getDateLivraison()+"',"+unClient.getSignatureBase64()+"',"+unClient.getEtat()+ ");";
        Log.d("log",req);
        bd.execSQL(req);
        bd.close();
    }

    public Client getClient(int idV){
        Client leClient = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from client where idC="+idV+";",null);
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            leClient = new Client(idV,curseur.getString(1), curseur.getString(2),curseur.getString(3),
                    curseur.getString(4),curseur.getString(5),curseur.getString(6),curseur.getString(7),
                    curseur.getString(8),curseur.getString(9),curseur.getString(10),curseur.getString(11),
                    curseur.getInt(12));
        }
        return leClient;
    }
    public void updateClient(int id, String label, int value){
        SQLiteDatabase bd = accesBD.getWritableDatabase();

        String req = "Update client SET " + label + " = " + value + " WHERE idC = " + id ;
        Log.d("log",req);
        try {
            bd.execSQL(req);
        }
        catch(android.database.sqlite.SQLiteConstraintException e){
            Log.d("sql :", e.getMessage());
        }

        bd.close();
    }

    public void updateClient(int id, String label, String value){
        SQLiteDatabase bd = accesBD.getWritableDatabase();

        String req = "Update client SET " + label + " = '" + value + "' WHERE idC = " + id ;
        Log.d("log",req);
        try {
            bd.execSQL(req);
        }
        catch(android.database.sqlite.SQLiteConstraintException e){
            Log.d("sql :", e.getMessage());
        }
        bd.close();
    }


}