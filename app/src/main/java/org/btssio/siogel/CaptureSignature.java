package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

public class CaptureSignature extends AppCompatActivity implements View.OnClickListener  {

    ClientDAO dao = new ClientDAO(this);
    LinearLayout vue_sign;
    Button C_sing_annuler, C_sign_effacer, C_sign_enrg;
    Client client;
    Signature laSignature;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_signature);

        int id = this.getIntent().getExtras().getInt("id");
        client = dao.getClient(id);

        vue_sign = (LinearLayout) findViewById(R.id.vue_sign);
        C_sing_annuler = (Button) findViewById(R.id.C_sign_annuler);
        C_sign_effacer = (Button) findViewById(R.id.C_sign_Effecer);
        C_sign_enrg = (Button) findViewById(R.id.C_sign_Enrg);
        C_sign_enrg.setClickable(false);

        C_sing_annuler.setOnClickListener(this);
        C_sign_effacer.setOnClickListener(this);
        C_sign_enrg.setOnClickListener(this);

        String ligne1 = "client " + client.getIdentifiant() + " - " + client.getPrenom() + " " +client.getNom();
        String ligne2 = "livré le " + client.getDateLivraison() + " à " + client.getHeureReelleLivraison();

        laSignature = new Signature(this,null,ligne1,ligne2);
        vue_sign.addView(laSignature, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.C_sign_annuler:
                finish();
                break;
            case R.id.C_sign_Effecer:
                laSignature.reset();
                C_sign_enrg.setClickable(false);

                break;
            case R.id.C_sign_Enrg:
                client.setSignatureBase64(laSignature.save());
                ClientDAO dao = new ClientDAO(this);
                dao.updateClient(client.getIdentifiant(), "signatureBase64",client.getSignatureBase64());
                finish();
                break;
        }
    }

    public class Signature extends View {
        // variables nécessaire au dessin
        private Paint paint = new Paint();
        private Path path = new Path();// collection de l'ensemble des points sauvegardés lors des mouvements du doigt
        private Canvas canvas;
        private Bitmap bitmap;
        private String ligne1;
        private String ligne2;
        // constructeur
        public Signature(Context context, AttributeSet attrs, String l1,String l2) {
            super(context, attrs);
            this.ligne1=l1;
            this.ligne2=l2;
            this.setBackgroundColor(Color.GRAY);
            paint.setColor(Color.WHITE); // couleur du tracé
            paint.setStrokeWidth(5f); //taille de la grosseur du trait en pixel
            paint.setTextSize(20);// taille du texte pour afficher les lignes
        }
        //gestion du dessin
        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawText(ligne1, 20, 30, paint);
            canvas.drawText(ligne2, 20, 60, paint);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(path, paint);
        }
        // gestion des événements du doigt
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            C_sign_enrg.setClickable(true);
            float eventX = event.getX();
            float eventY = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    return true;
                case MotionEvent.ACTION_MOVE:
                    path.lineTo(eventX, eventY);
                    break;
                case MotionEvent.ACTION_UP:
// nothing to do
                    break;
                default:
                    return false;
            }
            invalidate();
            return true;
        }
        public String save() {
            String vretour;
            if (bitmap == null) { // si l’objet bitmap n’est pas initialisé
// il faut l’initialiser avec les tailles définie dans la vue du contexte
                bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(),

                        Bitmap.Config.RGB_565);

            }
            try {
//
                canvas = new Canvas(bitmap);
                this.draw(canvas); // dessine la vue
// la classe ByteArrayOutputStream implémente un flux de sortie dans lequel les
// données sont écrites dans un tableau d'octets. La mémoire tampon augmente
// automatiquement à mesure que des données y sont écrites :
                ByteArrayOutputStream ByteStream = new ByteArrayOutputStream();
// Ecrit une version compressée du bitmap dans le flux de sortie défini précédemment
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ByteStream);
// Création d’un tableau d'octets. Sa taille est la taille actuelle du flux de sortie.
                byte[] b = ByteStream.toByteArray();
// ce tableau est ensuite codé en String (utilisation pour cela d’un utilitaire de
// codage/décodage de la représentation Base64 de données binaires) :
                vretour = Base64.encodeToString(b, Base64.DEFAULT);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Erreur Sauvegarde",

                        Toast.LENGTH_LONG).show();

                vretour = null;
            }
            return vretour;

        }

        public void reset() {
            path.reset();
            invalidate();
        }
    }
}