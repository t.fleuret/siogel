package org.btssio.siogel;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ClientAdapter extends BaseAdapter {
    List<Client> listeClients = new ArrayList<Client>();
    LayoutInflater layoutInflater;

    public ClientAdapter(Context context,List<Client> listeClients) {
        this.listeClients = listeClients;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public ClientAdapter(List<Client> listeClients, LayoutInflater layoutInflater) {
        this.listeClients = listeClients;
        this.layoutInflater = layoutInflater;
    }

    @Override
    public int getCount() {

        return listeClients.size();
    }

    @Override
    public Object getItem(int i) {

        return listeClients.get(i);
    }

    @Override
    public long getItemId(int i) {

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Conteneur contenu;

        if (convertView == null) {
            contenu = new Conteneur();
            convertView = layoutInflater.inflate(R.layout.vue_client, null);
            contenu.txtId = (TextView) convertView.findViewById(R.id.identifiant);
            contenu.txtNom = (TextView) convertView.findViewById(R.id.nom);
            contenu.txtPrenom = (TextView) convertView.findViewById(R.id.prenom);
            contenu.txtTelephone = (TextView) convertView.findViewById(R.id.telephone);
            contenu.txtAdresse = (TextView) convertView.findViewById(R.id.adresse);
            contenu.txtCodepPostal = (TextView) convertView.findViewById(R.id.CP);
            contenu.txtVille = (TextView) convertView.findViewById(R.id.Ville);
            convertView.setTag(contenu);
        }
        else {
            contenu = (Conteneur) convertView.getTag();
        }
        contenu.txtId.setText(""+listeClients.get(position).getIdentifiant());
        contenu.txtNom.setText(""+listeClients.get(position).getNom());
        contenu.txtPrenom.setText(""+listeClients.get(position).getPrenom());
        contenu.txtAdresse.setText(""+listeClients.get(position).getAdresse());
        contenu.txtCodepPostal.setText(""+listeClients.get(position).getCodePostal());
        contenu.txtVille.setText(""+listeClients.get(position).getVille());

        String tel = listeClients.get(position).getTelephone();
        try {
            tel = String.format("%s.%s.%s.%s.%s",
                    '0' + tel.substring(0, 1),
                    tel.substring(1, 3),
                    tel.substring(3, 5),
                    tel.substring(5, 7),
                    tel.substring(7,9));

            contenu.txtTelephone.setText(tel);
        }
        catch (Exception e){
            Log.d("5800", listeClients.get(position).getTelephone() + "  : " + e.getMessage());
        }

        if (listeClients.get(position).getEtat() == 0) { //non livrée
            convertView.setBackgroundColor(Color.rgb(238,233,233));
        }
        else if (listeClients.get(position).getEtat() == 1 ) //abs
        {
            convertView.setBackgroundColor(Color.rgb(243,236,24));
        }
        else if (listeClients.get(position).getEtat() == 3 ) // livrée
        {
            convertView.setBackgroundColor(Color.rgb(0,255,0));
        }
        else // problème
        {
            convertView.setBackgroundColor(Color.rgb(255,0,0));
        }

        return convertView;
    }

    public class Conteneur {
        TextView txtId;
        TextView txtNom;
        TextView txtPrenom;
        TextView txtTelephone;
        TextView txtAdresse;
        TextView txtCodepPostal;
        TextView txtVille;

        public TextView getTxtId() {
            return txtId;
        }
    }
}
